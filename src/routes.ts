import { FastifyInstance } from 'fastify'
import { TagBuilderController } from './controllers/TagBuilderController';


export async function appRoutes(app: FastifyInstance) {
  app.post('/tags', new TagBuilderController().create);
}