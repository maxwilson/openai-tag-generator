interface ImportMeta {
  env: {
    NODE_ENV: 'development' | 'production';
    PORT: Number;
    OPENAI_API_KEY: string;
    OPENAI_ORGANIZATION: string;
  };
}