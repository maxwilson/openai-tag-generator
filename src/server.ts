import fastify from "fastify";
import cors from '@fastify/cors';
import { appRoutes } from "./routes";
import "./libs/dotEnv";

const app = fastify();

app.register(cors);
app.register(appRoutes);

const PORT = 3333;

app.listen({
  port: PORT,
  host: '0.0.0.0'
}).then(() => {
  console.log(`HTTP Server running on http://localhost:${PORT}`);
});