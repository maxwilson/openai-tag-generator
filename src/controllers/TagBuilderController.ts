import { FastifyReply, FastifyRequest } from "fastify";
import { Configuration, OpenAIApi } from "openai";
import { z } from "zod";

export class TagBuilderController {
  async create(req: FastifyRequest, rep: FastifyReply) {

    const body = z.object({
      title: z.string().max(100).min(10).trim(),
    });

    const { title } = body.parse(req.body);

    try {

      const config = new Configuration({
        apiKey: process.env.OPENAI_API_KEY,
        organization: process.env.OPENAI_ORGANIZATION
      });

      const openai = new OpenAIApi(config);

      const prompt = `
        Generate keywords in Portuguese for a post about ${title.toString().trim()}.
        Replace the spaces in each word with the character "_".
        Return each item separated by a comma, in lowercase, and without a line break.
      `;

      const completion = await openai.createCompletion({
        model: 'text-davinci-003',
        prompt: prompt,
        temperature: 0.7,
        max_tokens: 500,
        top_p: 1,
        frequency_penalty: 0,
        presence_penalty: 0
      });

      let hashtags: Array<string | null> = [];
      if (completion.data.choices[0].text) {
        let data = completion.data.choices[0].text?.trim().split(',');
        data.forEach(tag => {
          hashtags.push(`#${tag.trim()}`);
        });
      }

      return rep.status(200).send({ "hashtags": hashtags });
    } catch (error) {
      console.log(error);
      return rep.status(500).send({ message: "internal server error" });
    }
  }
}