# Tag Generator

Tag Generator é uma API para gerar tags para criadores de conteúdo. Com ela é possível gerar tags relevantes para serem utilizadas em redes sociais como Instagram, Twitter, entre outras.

## Funcionalidades
 - Geração de hashtags

## Instalação

Antes de executar o projeto, é necessário ter o Node.js instalados na sua máquina. Em seguida, siga os passos abaixo:

1. Clone o repositório do projeto:

```bash
git clone https://gitlab.com/maxwilson/openai-generate-tags
```

2. Na raiz do projeto, execute o seguinte comando para instalar as dependências:

```bash
npm install
```

## Configuração

Antes de rodar o projeto, é necessário configurar as variáveis de ambiente. Para isso, crie um arquivo .env na raiz do projeto com as seguintes variáveis:

```bash
NODE_ENV="development"
PORT=
OPENAI_API_KEY=""
OPENAI_ORGANIZATION=""
```

## Uso
Para iniciar a API, basta rodar o seguinte comando:

```bash
npm run dev
```
A API estará disponível na porta configurada na variável de ambiente PORT. Para gerar tags, basta enviar uma requisição HTTP POST para a rota /tags com um objeto JSON contendo as informações do post. Por exemplo:

```bash
POST /tags
{
  "title": "tutorial como instalar mod no gta San Andreas",
}
```
A resposta será um objeto JSON contendo as tags geradas:
```bash
HTTP/1.1 200 OK
{
  "hashtags": ["#gtaSa", "#Tutorial_Mod_GTASA", "#GTASAMODS"]
}
```
## Tecnologias utilizadas
- Node.js
- TypeScript
- Fastify
- Zod

## Licença
Esse projeto está sob a licença MIT. Veja o arquivo [LICENSE](LICENSE) para mais detalhes.
